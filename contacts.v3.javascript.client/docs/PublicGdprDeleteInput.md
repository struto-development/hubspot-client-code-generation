# Contacts.PublicGdprDeleteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**objectId** | **String** |  | 
**idProperty** | **String** |  | [optional] 


