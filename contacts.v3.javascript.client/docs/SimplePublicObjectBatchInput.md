# Contacts.SimplePublicObjectBatchInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | **{String: String}** |  | 
**id** | **String** |  | 


