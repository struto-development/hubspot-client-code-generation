# Contacts.StandardError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**id** | **String** |  | [optional] 
**category** | [**ErrorCategory**](ErrorCategory.md) |  | 
**subCategory** | **Object** |  | [optional] 
**message** | **String** |  | 
**errors** | [**[ErrorDetail]**](ErrorDetail.md) |  | 
**context** | **{String: [String]}** |  | 
**links** | **{String: String}** |  | 


