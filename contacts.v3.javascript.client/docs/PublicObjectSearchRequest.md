# Contacts.PublicObjectSearchRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filterGroups** | [**[FilterGroup]**](FilterGroup.md) |  | 
**sorts** | **[String]** |  | 
**query** | **String** |  | [optional] 
**properties** | **[String]** |  | 
**limit** | **Number** |  | 
**after** | **Number** |  | 


