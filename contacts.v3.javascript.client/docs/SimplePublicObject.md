# Contacts.SimplePublicObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**properties** | **{String: String}** |  | 
**createdAt** | **Date** |  | 
**updatedAt** | **Date** |  | 
**archived** | **Boolean** |  | [optional] 
**archivedAt** | **Date** |  | [optional] 


