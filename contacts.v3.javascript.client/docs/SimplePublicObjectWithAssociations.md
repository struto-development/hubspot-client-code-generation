# Contacts.SimplePublicObjectWithAssociations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**properties** | **{String: String}** |  | 
**createdAt** | **Date** |  | 
**updatedAt** | **Date** |  | 
**archived** | **Boolean** |  | [optional] 
**archivedAt** | **Date** |  | [optional] 
**associations** | [**{String: CollectionResponseAssociatedId}**](CollectionResponseAssociatedId.md) |  | [optional] 


