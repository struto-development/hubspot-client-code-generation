# Contacts.AssociationsApi

All URIs are relative to *https://api.hubapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive**](AssociationsApi.md#deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive) | **DELETE** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Remove an association between two contacts
[**getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll**](AssociationsApi.md#getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll) | **GET** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType} | List associations of a contact by type
[**putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate**](AssociationsApi.md#putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate) | **PUT** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Associate a contact with another object



## deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive

> deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive(contactId, toObjectType, toObjectId, associationType)

Remove an association between two contacts

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.AssociationsApi();
let contactId = "contactId_example"; // String | 
let toObjectType = "toObjectType_example"; // String | 
let toObjectId = "toObjectId_example"; // String | 
let associationType = "associationType_example"; // String | 
apiInstance.deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive(contactId, toObjectType, toObjectId, associationType, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactId** | **String**|  | 
 **toObjectType** | **String**|  | 
 **toObjectId** | **String**|  | 
 **associationType** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll

> CollectionResponseAssociatedIdForwardPaging getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll(contactId, toObjectType, opts)

List associations of a contact by type

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.AssociationsApi();
let contactId = "contactId_example"; // String | 
let toObjectType = "toObjectType_example"; // String | 
let opts = {
  'after': "after_example", // String | The paging cursor token of the last successfully read resource will be returned as the `paging.next.after` JSON property of a paged response containing more results.
  'limit': 500 // Number | The maximum number of results to display per page.
};
apiInstance.getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll(contactId, toObjectType, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactId** | **String**|  | 
 **toObjectType** | **String**|  | 
 **after** | **String**| The paging cursor token of the last successfully read resource will be returned as the &#x60;paging.next.after&#x60; JSON property of a paged response containing more results. | [optional] 
 **limit** | **Number**| The maximum number of results to display per page. | [optional] [default to 500]

### Return type

[**CollectionResponseAssociatedIdForwardPaging**](CollectionResponseAssociatedIdForwardPaging.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, */*


## putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate

> SimplePublicObjectWithAssociations putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate(contactId, toObjectType, toObjectId, associationType)

Associate a contact with another object

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.AssociationsApi();
let contactId = "contactId_example"; // String | 
let toObjectType = "toObjectType_example"; // String | 
let toObjectId = "toObjectId_example"; // String | 
let associationType = "associationType_example"; // String | 
apiInstance.putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate(contactId, toObjectType, toObjectId, associationType, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactId** | **String**|  | 
 **toObjectType** | **String**|  | 
 **toObjectId** | **String**|  | 
 **associationType** | **String**|  | 

### Return type

[**SimplePublicObjectWithAssociations**](SimplePublicObjectWithAssociations.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, */*

