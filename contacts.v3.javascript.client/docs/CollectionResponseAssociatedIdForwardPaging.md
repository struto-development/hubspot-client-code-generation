# Contacts.CollectionResponseAssociatedIdForwardPaging

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[AssociatedId]**](AssociatedId.md) |  | 
**paging** | [**ForwardPaging**](ForwardPaging.md) |  | [optional] 


