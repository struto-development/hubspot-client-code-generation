# Contacts.NextPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**after** | **String** |  | 
**link** | **String** |  | [optional] 


