# Contacts.CollectionResponseSimplePublicObjectWithAssociationsForwardPaging

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[SimplePublicObjectWithAssociations]**](SimplePublicObjectWithAssociations.md) |  | 
**paging** | [**ForwardPaging**](ForwardPaging.md) |  | [optional] 


