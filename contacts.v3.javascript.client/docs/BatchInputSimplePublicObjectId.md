# Contacts.BatchInputSimplePublicObjectId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**[SimplePublicObjectId]**](SimplePublicObjectId.md) |  | 


