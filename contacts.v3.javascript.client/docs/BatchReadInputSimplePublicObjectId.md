# Contacts.BatchReadInputSimplePublicObjectId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | **[String]** |  | 
**idProperty** | **String** |  | [optional] 
**inputs** | [**[SimplePublicObjectId]**](SimplePublicObjectId.md) |  | 


