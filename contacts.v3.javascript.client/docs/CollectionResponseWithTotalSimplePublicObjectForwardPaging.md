# Contacts.CollectionResponseWithTotalSimplePublicObjectForwardPaging

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Number** |  | 
**results** | [**[SimplePublicObject]**](SimplePublicObject.md) |  | 
**paging** | [**ForwardPaging**](ForwardPaging.md) |  | [optional] 


