# Contacts.BatchResponseSimplePublicObjectWithErrors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**results** | [**[SimplePublicObject]**](SimplePublicObject.md) |  | 
**numErrors** | **Number** |  | [optional] 
**errors** | [**[StandardError]**](StandardError.md) |  | [optional] 
**requestedAt** | **Date** |  | [optional] 
**startedAt** | **Date** |  | 
**completedAt** | **Date** |  | 
**links** | **{String: String}** |  | [optional] 



## Enum: StatusEnum


* `PENDING` (value: `"PENDING"`)

* `PROCESSING` (value: `"PROCESSING"`)

* `CANCELED` (value: `"CANCELED"`)

* `COMPLETE` (value: `"COMPLETE"`)




