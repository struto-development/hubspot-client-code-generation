# Contacts.BatchResponseSimplePublicObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**results** | [**[SimplePublicObject]**](SimplePublicObject.md) |  | 
**requestedAt** | **Date** |  | [optional] 
**startedAt** | **Date** |  | 
**completedAt** | **Date** |  | 
**links** | **{String: String}** |  | [optional] 



## Enum: StatusEnum


* `PENDING` (value: `"PENDING"`)

* `PROCESSING` (value: `"PROCESSING"`)

* `CANCELED` (value: `"CANCELED"`)

* `COMPLETE` (value: `"COMPLETE"`)




