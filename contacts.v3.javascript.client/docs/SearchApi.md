# Contacts.SearchApi

All URIs are relative to *https://api.hubapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postCrmV3ObjectsContactsSearchDoSearch**](SearchApi.md#postCrmV3ObjectsContactsSearchDoSearch) | **POST** /crm/v3/objects/contacts/search | 



## postCrmV3ObjectsContactsSearchDoSearch

> CollectionResponseWithTotalSimplePublicObjectForwardPaging postCrmV3ObjectsContactsSearchDoSearch(publicObjectSearchRequest)



### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.SearchApi();
let publicObjectSearchRequest = new Contacts.PublicObjectSearchRequest(); // PublicObjectSearchRequest | 
apiInstance.postCrmV3ObjectsContactsSearchDoSearch(publicObjectSearchRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **publicObjectSearchRequest** | [**PublicObjectSearchRequest**](PublicObjectSearchRequest.md)|  | 

### Return type

[**CollectionResponseWithTotalSimplePublicObjectForwardPaging**](CollectionResponseWithTotalSimplePublicObjectForwardPaging.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, */*

