# Contacts.ErrorDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** | A human readable message describing the error along with remediation steps where appropriate | 
**_in** | **String** | The name of the field or parameter in which the error was found. | [optional] 
**code** | **String** | The status code associated with the error detail | [optional] 
**subCategory** | **String** | A specific category that contains more specific detail about the error | [optional] 
**context** | **{String: [String]}** | Context about the error condition | [optional] 


