# Contacts.BasicApi

All URIs are relative to *https://api.hubapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCrmV3ObjectsContactsContactIdArchive**](BasicApi.md#deleteCrmV3ObjectsContactsContactIdArchive) | **DELETE** /crm/v3/objects/contacts/{contactId} | Archive
[**getCrmV3ObjectsContactsContactIdGetById**](BasicApi.md#getCrmV3ObjectsContactsContactIdGetById) | **GET** /crm/v3/objects/contacts/{contactId} | Read
[**getCrmV3ObjectsContactsGetPage**](BasicApi.md#getCrmV3ObjectsContactsGetPage) | **GET** /crm/v3/objects/contacts | List
[**patchCrmV3ObjectsContactsContactIdUpdate**](BasicApi.md#patchCrmV3ObjectsContactsContactIdUpdate) | **PATCH** /crm/v3/objects/contacts/{contactId} | Update
[**postCrmV3ObjectsContactsCreate**](BasicApi.md#postCrmV3ObjectsContactsCreate) | **POST** /crm/v3/objects/contacts | Create



## deleteCrmV3ObjectsContactsContactIdArchive

> deleteCrmV3ObjectsContactsContactIdArchive(contactId)

Archive

Move an Object identified by &#x60;{contactId}&#x60; to the recycling bin.

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BasicApi();
let contactId = "contactId_example"; // String | 
apiInstance.deleteCrmV3ObjectsContactsContactIdArchive(contactId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactId** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getCrmV3ObjectsContactsContactIdGetById

> SimplePublicObjectWithAssociations getCrmV3ObjectsContactsContactIdGetById(contactId, opts)

Read

Read an Object identified by &#x60;{contactId}&#x60;. &#x60;{contactId}&#x60; refers to the internal object ID by default, or optionally any unique property value as specified by the &#x60;idProperty&#x60; query param.  Control what is returned via the &#x60;properties&#x60; query param.

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BasicApi();
let contactId = "contactId_example"; // String | 
let opts = {
  'properties': ["null"], // [String] | A comma separated list of the properties to be returned in the response. If any of the specified properties are not present on the requested object(s), they will be ignored.
  'associations': ["null"], // [String] | A comma separated list of object types to retrieve associated IDs for. If any of the specified associations do not exist, they will be ignored.
  'archived': false, // Boolean | Whether to return only results that have been archived.
  'idProperty': "idProperty_example" // String | The name of a property whose values are unique for this object type
};
apiInstance.getCrmV3ObjectsContactsContactIdGetById(contactId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactId** | **String**|  | 
 **properties** | [**[String]**](String.md)| A comma separated list of the properties to be returned in the response. If any of the specified properties are not present on the requested object(s), they will be ignored. | [optional] 
 **associations** | [**[String]**](String.md)| A comma separated list of object types to retrieve associated IDs for. If any of the specified associations do not exist, they will be ignored. | [optional] 
 **archived** | **Boolean**| Whether to return only results that have been archived. | [optional] [default to false]
 **idProperty** | **String**| The name of a property whose values are unique for this object type | [optional] 

### Return type

[**SimplePublicObjectWithAssociations**](SimplePublicObjectWithAssociations.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, */*


## getCrmV3ObjectsContactsGetPage

> CollectionResponseSimplePublicObjectWithAssociationsForwardPaging getCrmV3ObjectsContactsGetPage(opts)

List

Read a page of contacts. Control what is returned via the &#x60;properties&#x60; query param.

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BasicApi();
let opts = {
  'limit': 10, // Number | The maximum number of results to display per page.
  'after': "after_example", // String | The paging cursor token of the last successfully read resource will be returned as the `paging.next.after` JSON property of a paged response containing more results.
  'properties': ["null"], // [String] | A comma separated list of the properties to be returned in the response. If any of the specified properties are not present on the requested object(s), they will be ignored.
  'associations': ["null"], // [String] | A comma separated list of object types to retrieve associated IDs for. If any of the specified associations do not exist, they will be ignored.
  'archived': false // Boolean | Whether to return only results that have been archived.
};
apiInstance.getCrmV3ObjectsContactsGetPage(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Number**| The maximum number of results to display per page. | [optional] [default to 10]
 **after** | **String**| The paging cursor token of the last successfully read resource will be returned as the &#x60;paging.next.after&#x60; JSON property of a paged response containing more results. | [optional] 
 **properties** | [**[String]**](String.md)| A comma separated list of the properties to be returned in the response. If any of the specified properties are not present on the requested object(s), they will be ignored. | [optional] 
 **associations** | [**[String]**](String.md)| A comma separated list of object types to retrieve associated IDs for. If any of the specified associations do not exist, they will be ignored. | [optional] 
 **archived** | **Boolean**| Whether to return only results that have been archived. | [optional] [default to false]

### Return type

[**CollectionResponseSimplePublicObjectWithAssociationsForwardPaging**](CollectionResponseSimplePublicObjectWithAssociationsForwardPaging.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, */*


## patchCrmV3ObjectsContactsContactIdUpdate

> SimplePublicObject patchCrmV3ObjectsContactsContactIdUpdate(contactId, simplePublicObjectInput, opts)

Update

Perform a partial update of an Object identified by &#x60;{contactId}&#x60;. &#x60;{contactId}&#x60; refers to the internal object ID by default, or optionally any unique property value as specified by the &#x60;idProperty&#x60; query param. Provided property values will be overwritten. Read-only and non-existent properties will be ignored. Properties values can be cleared by passing an empty string.

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BasicApi();
let contactId = "contactId_example"; // String | 
let simplePublicObjectInput = new Contacts.SimplePublicObjectInput(); // SimplePublicObjectInput | 
let opts = {
  'idProperty': "idProperty_example" // String | The name of a property whose values are unique for this object type
};
apiInstance.patchCrmV3ObjectsContactsContactIdUpdate(contactId, simplePublicObjectInput, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactId** | **String**|  | 
 **simplePublicObjectInput** | [**SimplePublicObjectInput**](SimplePublicObjectInput.md)|  | 
 **idProperty** | **String**| The name of a property whose values are unique for this object type | [optional] 

### Return type

[**SimplePublicObject**](SimplePublicObject.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, */*


## postCrmV3ObjectsContactsCreate

> SimplePublicObject postCrmV3ObjectsContactsCreate(simplePublicObjectInput)

Create

Create a contact with the given properties and return a copy of the object, including the ID. Documentation and examples for creating standard contacts is provided.

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BasicApi();
let simplePublicObjectInput = new Contacts.SimplePublicObjectInput(); // SimplePublicObjectInput | 
apiInstance.postCrmV3ObjectsContactsCreate(simplePublicObjectInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplePublicObjectInput** | [**SimplePublicObjectInput**](SimplePublicObjectInput.md)|  | 

### Return type

[**SimplePublicObject**](SimplePublicObject.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, */*

