# Contacts.Filter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** |  | [optional] 
**propertyName** | **String** |  | 
**operator** | **String** | null | 



## Enum: OperatorEnum


* `EQ` (value: `"EQ"`)

* `NEQ` (value: `"NEQ"`)

* `LT` (value: `"LT"`)

* `LTE` (value: `"LTE"`)

* `GT` (value: `"GT"`)

* `GTE` (value: `"GTE"`)

* `BETWEEN` (value: `"BETWEEN"`)

* `IN` (value: `"IN"`)

* `NOT_IN` (value: `"NOT_IN"`)

* `HAS_PROPERTY` (value: `"HAS_PROPERTY"`)

* `NOT_HAS_PROPERTY` (value: `"NOT_HAS_PROPERTY"`)

* `CONTAINS_TOKEN` (value: `"CONTAINS_TOKEN"`)

* `NOT_CONTAINS_TOKEN` (value: `"NOT_CONTAINS_TOKEN"`)




