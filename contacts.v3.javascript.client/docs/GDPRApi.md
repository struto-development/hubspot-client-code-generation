# Contacts.GDPRApi

All URIs are relative to *https://api.hubapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postCrmV3ObjectsContactsGdprDelete**](GDPRApi.md#postCrmV3ObjectsContactsGdprDelete) | **POST** /crm/v3/objects/contacts/gdpr-delete | GDPR DELETE



## postCrmV3ObjectsContactsGdprDelete

> postCrmV3ObjectsContactsGdprDelete(publicGdprDeleteInput)

GDPR DELETE

Permanently delete a contact and all associated content to follow GDPR. Use optional property &#39;idProperty&#39; set to &#39;email&#39; to identify contact by email address. If email address is not found, the email address will be added to a blocklist and prevent it from being used in the future.

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.GDPRApi();
let publicGdprDeleteInput = new Contacts.PublicGdprDeleteInput(); // PublicGdprDeleteInput | 
apiInstance.postCrmV3ObjectsContactsGdprDelete(publicGdprDeleteInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **publicGdprDeleteInput** | [**PublicGdprDeleteInput**](PublicGdprDeleteInput.md)|  | 

### Return type

null (empty response body)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

