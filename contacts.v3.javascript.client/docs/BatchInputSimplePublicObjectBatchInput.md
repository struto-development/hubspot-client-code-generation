# Contacts.BatchInputSimplePublicObjectBatchInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**[SimplePublicObjectBatchInput]**](SimplePublicObjectBatchInput.md) |  | 


