# Contacts.BatchApi

All URIs are relative to *https://api.hubapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postCrmV3ObjectsContactsBatchArchiveArchive**](BatchApi.md#postCrmV3ObjectsContactsBatchArchiveArchive) | **POST** /crm/v3/objects/contacts/batch/archive | Archive a batch of contacts by ID
[**postCrmV3ObjectsContactsBatchCreateCreate**](BatchApi.md#postCrmV3ObjectsContactsBatchCreateCreate) | **POST** /crm/v3/objects/contacts/batch/create | Create a batch of contacts
[**postCrmV3ObjectsContactsBatchReadRead**](BatchApi.md#postCrmV3ObjectsContactsBatchReadRead) | **POST** /crm/v3/objects/contacts/batch/read | Read a batch of contacts by internal ID, or unique property values
[**postCrmV3ObjectsContactsBatchUpdateUpdate**](BatchApi.md#postCrmV3ObjectsContactsBatchUpdateUpdate) | **POST** /crm/v3/objects/contacts/batch/update | Update a batch of contacts



## postCrmV3ObjectsContactsBatchArchiveArchive

> postCrmV3ObjectsContactsBatchArchiveArchive(batchInputSimplePublicObjectId)

Archive a batch of contacts by ID

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BatchApi();
let batchInputSimplePublicObjectId = new Contacts.BatchInputSimplePublicObjectId(); // BatchInputSimplePublicObjectId | 
apiInstance.postCrmV3ObjectsContactsBatchArchiveArchive(batchInputSimplePublicObjectId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchInputSimplePublicObjectId** | [**BatchInputSimplePublicObjectId**](BatchInputSimplePublicObjectId.md)|  | 

### Return type

null (empty response body)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## postCrmV3ObjectsContactsBatchCreateCreate

> BatchResponseSimplePublicObject postCrmV3ObjectsContactsBatchCreateCreate(batchInputSimplePublicObjectInput)

Create a batch of contacts

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BatchApi();
let batchInputSimplePublicObjectInput = new Contacts.BatchInputSimplePublicObjectInput(); // BatchInputSimplePublicObjectInput | 
apiInstance.postCrmV3ObjectsContactsBatchCreateCreate(batchInputSimplePublicObjectInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchInputSimplePublicObjectInput** | [**BatchInputSimplePublicObjectInput**](BatchInputSimplePublicObjectInput.md)|  | 

### Return type

[**BatchResponseSimplePublicObject**](BatchResponseSimplePublicObject.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, */*


## postCrmV3ObjectsContactsBatchReadRead

> BatchResponseSimplePublicObject postCrmV3ObjectsContactsBatchReadRead(batchReadInputSimplePublicObjectId, opts)

Read a batch of contacts by internal ID, or unique property values

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BatchApi();
let batchReadInputSimplePublicObjectId = new Contacts.BatchReadInputSimplePublicObjectId(); // BatchReadInputSimplePublicObjectId | 
let opts = {
  'archived': false // Boolean | Whether to return only results that have been archived.
};
apiInstance.postCrmV3ObjectsContactsBatchReadRead(batchReadInputSimplePublicObjectId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchReadInputSimplePublicObjectId** | [**BatchReadInputSimplePublicObjectId**](BatchReadInputSimplePublicObjectId.md)|  | 
 **archived** | **Boolean**| Whether to return only results that have been archived. | [optional] [default to false]

### Return type

[**BatchResponseSimplePublicObject**](BatchResponseSimplePublicObject.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, */*


## postCrmV3ObjectsContactsBatchUpdateUpdate

> BatchResponseSimplePublicObject postCrmV3ObjectsContactsBatchUpdateUpdate(batchInputSimplePublicObjectBatchInput)

Update a batch of contacts

### Example

```javascript
import Contacts from 'contacts';
let defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
let hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix = 'Token';
// Configure OAuth2 access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = 'YOUR ACCESS TOKEN';
// Configure OAuth2 access token for authorization: oauth2_legacy
let oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new Contacts.BatchApi();
let batchInputSimplePublicObjectBatchInput = new Contacts.BatchInputSimplePublicObjectBatchInput(); // BatchInputSimplePublicObjectBatchInput | 
apiInstance.postCrmV3ObjectsContactsBatchUpdateUpdate(batchInputSimplePublicObjectBatchInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchInputSimplePublicObjectBatchInput** | [**BatchInputSimplePublicObjectBatchInput**](BatchInputSimplePublicObjectBatchInput.md)|  | 

### Return type

[**BatchResponseSimplePublicObject**](BatchResponseSimplePublicObject.md)

### Authorization

[hapikey](../README.md#hapikey), [oauth2](../README.md#oauth2), [oauth2_legacy](../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, */*

