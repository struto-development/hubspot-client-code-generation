# Contacts.CollectionResponseAssociatedId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[AssociatedId]**](AssociatedId.md) |  | 
**paging** | [**Paging**](Paging.md) |  | [optional] 


