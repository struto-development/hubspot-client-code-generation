# Contacts.BatchInputSimplePublicObjectInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**[SimplePublicObjectInput]**](SimplePublicObjectInput.md) |  | 


