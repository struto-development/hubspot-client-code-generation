/**
 * Contacts
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import FilterGroup from './FilterGroup';

/**
 * The PublicObjectSearchRequest model module.
 * @module model/PublicObjectSearchRequest
 * @version v3
 */
class PublicObjectSearchRequest {
    /**
     * Constructs a new <code>PublicObjectSearchRequest</code>.
     * @alias module:model/PublicObjectSearchRequest
     * @param filterGroups {Array.<module:model/FilterGroup>} 
     * @param sorts {Array.<String>} 
     * @param properties {Array.<String>} 
     * @param limit {Number} 
     * @param after {Number} 
     */
    constructor(filterGroups, sorts, properties, limit, after) { 
        
        PublicObjectSearchRequest.initialize(this, filterGroups, sorts, properties, limit, after);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, filterGroups, sorts, properties, limit, after) { 
        obj['filterGroups'] = filterGroups;
        obj['sorts'] = sorts;
        obj['properties'] = properties;
        obj['limit'] = limit;
        obj['after'] = after;
    }

    /**
     * Constructs a <code>PublicObjectSearchRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/PublicObjectSearchRequest} obj Optional instance to populate.
     * @return {module:model/PublicObjectSearchRequest} The populated <code>PublicObjectSearchRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PublicObjectSearchRequest();

            if (data.hasOwnProperty('filterGroups')) {
                obj['filterGroups'] = ApiClient.convertToType(data['filterGroups'], [FilterGroup]);
            }
            if (data.hasOwnProperty('sorts')) {
                obj['sorts'] = ApiClient.convertToType(data['sorts'], ['String']);
            }
            if (data.hasOwnProperty('query')) {
                obj['query'] = ApiClient.convertToType(data['query'], 'String');
            }
            if (data.hasOwnProperty('properties')) {
                obj['properties'] = ApiClient.convertToType(data['properties'], ['String']);
            }
            if (data.hasOwnProperty('limit')) {
                obj['limit'] = ApiClient.convertToType(data['limit'], 'Number');
            }
            if (data.hasOwnProperty('after')) {
                obj['after'] = ApiClient.convertToType(data['after'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Array.<module:model/FilterGroup>} filterGroups
 */
PublicObjectSearchRequest.prototype['filterGroups'] = undefined;

/**
 * @member {Array.<String>} sorts
 */
PublicObjectSearchRequest.prototype['sorts'] = undefined;

/**
 * @member {String} query
 */
PublicObjectSearchRequest.prototype['query'] = undefined;

/**
 * @member {Array.<String>} properties
 */
PublicObjectSearchRequest.prototype['properties'] = undefined;

/**
 * @member {Number} limit
 */
PublicObjectSearchRequest.prototype['limit'] = undefined;

/**
 * @member {Number} after
 */
PublicObjectSearchRequest.prototype['after'] = undefined;






export default PublicObjectSearchRequest;

