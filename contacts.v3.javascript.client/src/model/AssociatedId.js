/**
 * Contacts
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The AssociatedId model module.
 * @module model/AssociatedId
 * @version v3
 */
class AssociatedId {
    /**
     * Constructs a new <code>AssociatedId</code>.
     * @alias module:model/AssociatedId
     * @param id {String} 
     * @param type {String} 
     */
    constructor(id, type) { 
        
        AssociatedId.initialize(this, id, type);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, id, type) { 
        obj['id'] = id;
        obj['type'] = type;
    }

    /**
     * Constructs a <code>AssociatedId</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AssociatedId} obj Optional instance to populate.
     * @return {module:model/AssociatedId} The populated <code>AssociatedId</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new AssociatedId();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'String');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} id
 */
AssociatedId.prototype['id'] = undefined;

/**
 * @member {String} type
 */
AssociatedId.prototype['type'] = undefined;






export default AssociatedId;

