/**
 * Contacts
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import NextPage from './NextPage';

/**
 * The ForwardPaging model module.
 * @module model/ForwardPaging
 * @version v3
 */
class ForwardPaging {
    /**
     * Constructs a new <code>ForwardPaging</code>.
     * @alias module:model/ForwardPaging
     */
    constructor() { 
        
        ForwardPaging.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ForwardPaging</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ForwardPaging} obj Optional instance to populate.
     * @return {module:model/ForwardPaging} The populated <code>ForwardPaging</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ForwardPaging();

            if (data.hasOwnProperty('next')) {
                obj['next'] = NextPage.constructFromObject(data['next']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/NextPage} next
 */
ForwardPaging.prototype['next'] = undefined;






export default ForwardPaging;

