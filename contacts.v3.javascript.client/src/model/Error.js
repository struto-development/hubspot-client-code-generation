/**
 * Contacts
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import ErrorDetail from './ErrorDetail';

/**
 * The Error model module.
 * @module model/Error
 * @version v3
 */
class Error {
    /**
     * Constructs a new <code>Error</code>.
     * @alias module:model/Error
     * @param message {String} A human readable message describing the error along with remediation steps where appropriate
     * @param correlationId {String} A unique identifier for the request. Include this value with any error reports or support tickets
     * @param category {String} The error category
     */
    constructor(message, correlationId, category) { 
        
        Error.initialize(this, message, correlationId, category);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, message, correlationId, category) { 
        obj['message'] = message;
        obj['correlationId'] = correlationId;
        obj['category'] = category;
    }

    /**
     * Constructs a <code>Error</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Error} obj Optional instance to populate.
     * @return {module:model/Error} The populated <code>Error</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Error();

            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
            if (data.hasOwnProperty('correlationId')) {
                obj['correlationId'] = ApiClient.convertToType(data['correlationId'], 'String');
            }
            if (data.hasOwnProperty('category')) {
                obj['category'] = ApiClient.convertToType(data['category'], 'String');
            }
            if (data.hasOwnProperty('subCategory')) {
                obj['subCategory'] = ApiClient.convertToType(data['subCategory'], 'String');
            }
            if (data.hasOwnProperty('errors')) {
                obj['errors'] = ApiClient.convertToType(data['errors'], [ErrorDetail]);
            }
            if (data.hasOwnProperty('context')) {
                obj['context'] = ApiClient.convertToType(data['context'], {'String': ['String']});
            }
            if (data.hasOwnProperty('links')) {
                obj['links'] = ApiClient.convertToType(data['links'], {'String': 'String'});
            }
        }
        return obj;
    }


}

/**
 * A human readable message describing the error along with remediation steps where appropriate
 * @member {String} message
 */
Error.prototype['message'] = undefined;

/**
 * A unique identifier for the request. Include this value with any error reports or support tickets
 * @member {String} correlationId
 */
Error.prototype['correlationId'] = undefined;

/**
 * The error category
 * @member {String} category
 */
Error.prototype['category'] = undefined;

/**
 * A specific category that contains more specific detail about the error
 * @member {String} subCategory
 */
Error.prototype['subCategory'] = undefined;

/**
 * further information about the error
 * @member {Array.<module:model/ErrorDetail>} errors
 */
Error.prototype['errors'] = undefined;

/**
 * Context about the error condition
 * @member {Object.<String, Array.<String>>} context
 */
Error.prototype['context'] = undefined;

/**
 * A map of link names to associated URIs containing documentation about the error or recommended remediation steps
 * @member {Object.<String, String>} links
 */
Error.prototype['links'] = undefined;






export default Error;

