# contacts

Contacts - JavaScript client for contacts
No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
This SDK is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: v3
- Package version: v3
- Build package: org.openapitools.codegen.languages.JavascriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/), please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install contacts --save
```

Finally, you need to build the module:

```shell
npm run build
```

##### Local development

To use the library locally without publishing to a remote npm registry, first install the dependencies by changing into the directory containing `package.json` (and this README). Let's call this `JAVASCRIPT_CLIENT_DIR`. Then run:

```shell
npm install
```

Next, [link](https://docs.npmjs.com/cli/link) it globally in npm with the following, also from `JAVASCRIPT_CLIENT_DIR`:

```shell
npm link
```

To use the link you just defined in your project, switch to the directory you want to use your contacts from, and run:

```shell
npm link /path/to/<JAVASCRIPT_CLIENT_DIR>
```

Finally, you need to build the module:

```shell
npm run build
```

#### git

If the library is hosted at a git repository, e.g.https://github.com/GIT_USER_ID/GIT_REPO_ID
then install it via:

```shell
    npm install GIT_USER_ID/GIT_REPO_ID --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var Contacts = require('contacts');

var defaultClient = Contacts.ApiClient.instance;
// Configure API key authorization: hapikey
var hapikey = defaultClient.authentications['hapikey'];
hapikey.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//hapikey.apiKeyPrefix['hapikey'] = "Token"
// Configure OAuth2 access token for authorization: oauth2
var oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"
// Configure OAuth2 access token for authorization: oauth2_legacy
var oauth2_legacy = defaultClient.authentications['oauth2_legacy'];
oauth2_legacy.accessToken = "YOUR ACCESS TOKEN"

var api = new Contacts.AssociationsApi()
var contactId = "contactId_example"; // {String} 
var toObjectType = "toObjectType_example"; // {String} 
var toObjectId = "toObjectId_example"; // {String} 
var associationType = "associationType_example"; // {String} 
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive(contactId, toObjectType, toObjectId, associationType, callback);

```

## Documentation for API Endpoints

All URIs are relative to *https://api.hubapi.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*Contacts.AssociationsApi* | [**deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive**](docs/AssociationsApi.md#deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive) | **DELETE** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Remove an association between two contacts
*Contacts.AssociationsApi* | [**getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll**](docs/AssociationsApi.md#getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll) | **GET** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType} | List associations of a contact by type
*Contacts.AssociationsApi* | [**putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate**](docs/AssociationsApi.md#putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate) | **PUT** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Associate a contact with another object
*Contacts.BasicApi* | [**deleteCrmV3ObjectsContactsContactIdArchive**](docs/BasicApi.md#deleteCrmV3ObjectsContactsContactIdArchive) | **DELETE** /crm/v3/objects/contacts/{contactId} | Archive
*Contacts.BasicApi* | [**getCrmV3ObjectsContactsContactIdGetById**](docs/BasicApi.md#getCrmV3ObjectsContactsContactIdGetById) | **GET** /crm/v3/objects/contacts/{contactId} | Read
*Contacts.BasicApi* | [**getCrmV3ObjectsContactsGetPage**](docs/BasicApi.md#getCrmV3ObjectsContactsGetPage) | **GET** /crm/v3/objects/contacts | List
*Contacts.BasicApi* | [**patchCrmV3ObjectsContactsContactIdUpdate**](docs/BasicApi.md#patchCrmV3ObjectsContactsContactIdUpdate) | **PATCH** /crm/v3/objects/contacts/{contactId} | Update
*Contacts.BasicApi* | [**postCrmV3ObjectsContactsCreate**](docs/BasicApi.md#postCrmV3ObjectsContactsCreate) | **POST** /crm/v3/objects/contacts | Create
*Contacts.BatchApi* | [**postCrmV3ObjectsContactsBatchArchiveArchive**](docs/BatchApi.md#postCrmV3ObjectsContactsBatchArchiveArchive) | **POST** /crm/v3/objects/contacts/batch/archive | Archive a batch of contacts by ID
*Contacts.BatchApi* | [**postCrmV3ObjectsContactsBatchCreateCreate**](docs/BatchApi.md#postCrmV3ObjectsContactsBatchCreateCreate) | **POST** /crm/v3/objects/contacts/batch/create | Create a batch of contacts
*Contacts.BatchApi* | [**postCrmV3ObjectsContactsBatchReadRead**](docs/BatchApi.md#postCrmV3ObjectsContactsBatchReadRead) | **POST** /crm/v3/objects/contacts/batch/read | Read a batch of contacts by internal ID, or unique property values
*Contacts.BatchApi* | [**postCrmV3ObjectsContactsBatchUpdateUpdate**](docs/BatchApi.md#postCrmV3ObjectsContactsBatchUpdateUpdate) | **POST** /crm/v3/objects/contacts/batch/update | Update a batch of contacts
*Contacts.GDPRApi* | [**postCrmV3ObjectsContactsGdprDelete**](docs/GDPRApi.md#postCrmV3ObjectsContactsGdprDelete) | **POST** /crm/v3/objects/contacts/gdpr-delete | GDPR DELETE
*Contacts.SearchApi* | [**postCrmV3ObjectsContactsSearchDoSearch**](docs/SearchApi.md#postCrmV3ObjectsContactsSearchDoSearch) | **POST** /crm/v3/objects/contacts/search | 


## Documentation for Models

 - [Contacts.AssociatedId](docs/AssociatedId.md)
 - [Contacts.BatchInputSimplePublicObjectBatchInput](docs/BatchInputSimplePublicObjectBatchInput.md)
 - [Contacts.BatchInputSimplePublicObjectId](docs/BatchInputSimplePublicObjectId.md)
 - [Contacts.BatchInputSimplePublicObjectInput](docs/BatchInputSimplePublicObjectInput.md)
 - [Contacts.BatchReadInputSimplePublicObjectId](docs/BatchReadInputSimplePublicObjectId.md)
 - [Contacts.BatchResponseSimplePublicObject](docs/BatchResponseSimplePublicObject.md)
 - [Contacts.BatchResponseSimplePublicObjectWithErrors](docs/BatchResponseSimplePublicObjectWithErrors.md)
 - [Contacts.CollectionResponseAssociatedId](docs/CollectionResponseAssociatedId.md)
 - [Contacts.CollectionResponseAssociatedIdForwardPaging](docs/CollectionResponseAssociatedIdForwardPaging.md)
 - [Contacts.CollectionResponseSimplePublicObjectWithAssociationsForwardPaging](docs/CollectionResponseSimplePublicObjectWithAssociationsForwardPaging.md)
 - [Contacts.CollectionResponseWithTotalSimplePublicObjectForwardPaging](docs/CollectionResponseWithTotalSimplePublicObjectForwardPaging.md)
 - [Contacts.Error](docs/Error.md)
 - [Contacts.ErrorCategory](docs/ErrorCategory.md)
 - [Contacts.ErrorDetail](docs/ErrorDetail.md)
 - [Contacts.Filter](docs/Filter.md)
 - [Contacts.FilterGroup](docs/FilterGroup.md)
 - [Contacts.ForwardPaging](docs/ForwardPaging.md)
 - [Contacts.NextPage](docs/NextPage.md)
 - [Contacts.Paging](docs/Paging.md)
 - [Contacts.PreviousPage](docs/PreviousPage.md)
 - [Contacts.PublicGdprDeleteInput](docs/PublicGdprDeleteInput.md)
 - [Contacts.PublicObjectSearchRequest](docs/PublicObjectSearchRequest.md)
 - [Contacts.SimplePublicObject](docs/SimplePublicObject.md)
 - [Contacts.SimplePublicObjectBatchInput](docs/SimplePublicObjectBatchInput.md)
 - [Contacts.SimplePublicObjectId](docs/SimplePublicObjectId.md)
 - [Contacts.SimplePublicObjectInput](docs/SimplePublicObjectInput.md)
 - [Contacts.SimplePublicObjectWithAssociations](docs/SimplePublicObjectWithAssociations.md)
 - [Contacts.StandardError](docs/StandardError.md)


## Documentation for Authorization



### hapikey


- **Type**: API key
- **API key parameter name**: hapikey
- **Location**: URL query string



### oauth2


- **Type**: OAuth
- **Flow**: accessCode
- **Authorization URL**: https://app.hubspot.com/oauth/authorize
- **Scopes**: 
  - crm.objects.contacts.read:  
  - crm.objects.contacts.write:  



### oauth2_legacy


- **Type**: OAuth
- **Flow**: accessCode
- **Authorization URL**: https://app.hubspot.com/oauth/authorize
- **Scopes**: 
  - contacts: Read from and write to my Contacts

