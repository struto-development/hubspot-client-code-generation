<?php

/**
 * Contacts
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 * PHP version 7.2.5
 *
 * The version of the OpenAPI document: v3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI-Generator
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 * Source files are located at:
 *
 * > https://github.com/OpenAPITools/openapi-generator/blob/master/modules/openapi-generator/src/main/resources/php-laravel/
 */


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**
 * get getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll
 * Summary: List associations of a contact by type
 * Notes: 
 * Output-Formats: [application/json, *_/_*]
 */
Route::get('/crm/v3/objects/contacts/{contactId}/associations/{toObjectType}', 'AssociationsController@getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll');
/**
 * delete deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive
 * Summary: Remove an association between two contacts
 * Notes: 
 * Output-Formats: [*_/_*]
 */
Route::delete('/crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType}', 'AssociationsController@deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive');
/**
 * put putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate
 * Summary: Associate a contact with another object
 * Notes: 
 * Output-Formats: [application/json, *_/_*]
 */
Route::put('/crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType}', 'AssociationsController@putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate');
/**
 * get getCrmV3ObjectsContactsGetPage
 * Summary: List
 * Notes: Read a page of contacts. Control what is returned via the &#x60;properties&#x60; query param.
 * Output-Formats: [application/json, *_/_*]
 */
Route::get('/crm/v3/objects/contacts', 'BasicController@getCrmV3ObjectsContactsGetPage');
/**
 * post postCrmV3ObjectsContactsCreate
 * Summary: Create
 * Notes: Create a contact with the given properties and return a copy of the object, including the ID. Documentation and examples for creating standard contacts is provided.
 * Output-Formats: [application/json, *_/_*]
 */
Route::post('/crm/v3/objects/contacts', 'BasicController@postCrmV3ObjectsContactsCreate');
/**
 * delete deleteCrmV3ObjectsContactsContactIdArchive
 * Summary: Archive
 * Notes: Move an Object identified by &#x60;{contactId}&#x60; to the recycling bin.
 * Output-Formats: [*_/_*]
 */
Route::delete('/crm/v3/objects/contacts/{contactId}', 'BasicController@deleteCrmV3ObjectsContactsContactIdArchive');
/**
 * get getCrmV3ObjectsContactsContactIdGetById
 * Summary: Read
 * Notes: Read an Object identified by &#x60;{contactId}&#x60;. &#x60;{contactId}&#x60; refers to the internal object ID by default, or optionally any unique property value as specified by the &#x60;idProperty&#x60; query param.  Control what is returned via the &#x60;properties&#x60; query param.
 * Output-Formats: [application/json, *_/_*]
 */
Route::get('/crm/v3/objects/contacts/{contactId}', 'BasicController@getCrmV3ObjectsContactsContactIdGetById');
/**
 * patch patchCrmV3ObjectsContactsContactIdUpdate
 * Summary: Update
 * Notes: Perform a partial update of an Object identified by &#x60;{contactId}&#x60;. &#x60;{contactId}&#x60; refers to the internal object ID by default, or optionally any unique property value as specified by the &#x60;idProperty&#x60; query param. Provided property values will be overwritten. Read-only and non-existent properties will be ignored. Properties values can be cleared by passing an empty string.
 * Output-Formats: [application/json, *_/_*]
 */
Route::patch('/crm/v3/objects/contacts/{contactId}', 'BasicController@patchCrmV3ObjectsContactsContactIdUpdate');
/**
 * post postCrmV3ObjectsContactsBatchArchiveArchive
 * Summary: Archive a batch of contacts by ID
 * Notes: 
 * Output-Formats: [*_/_*]
 */
Route::post('/crm/v3/objects/contacts/batch/archive', 'BatchController@postCrmV3ObjectsContactsBatchArchiveArchive');
/**
 * post postCrmV3ObjectsContactsBatchCreateCreate
 * Summary: Create a batch of contacts
 * Notes: 
 * Output-Formats: [application/json, *_/_*]
 */
Route::post('/crm/v3/objects/contacts/batch/create', 'BatchController@postCrmV3ObjectsContactsBatchCreateCreate');
/**
 * post postCrmV3ObjectsContactsBatchReadRead
 * Summary: Read a batch of contacts by internal ID, or unique property values
 * Notes: 
 * Output-Formats: [application/json, *_/_*]
 */
Route::post('/crm/v3/objects/contacts/batch/read', 'BatchController@postCrmV3ObjectsContactsBatchReadRead');
/**
 * post postCrmV3ObjectsContactsBatchUpdateUpdate
 * Summary: Update a batch of contacts
 * Notes: 
 * Output-Formats: [application/json, *_/_*]
 */
Route::post('/crm/v3/objects/contacts/batch/update', 'BatchController@postCrmV3ObjectsContactsBatchUpdateUpdate');
/**
 * post postCrmV3ObjectsContactsGdprDelete
 * Summary: GDPR DELETE
 * Notes: Permanently delete a contact and all associated content to follow GDPR. Use optional property &#39;idProperty&#39; set to &#39;email&#39; to identify contact by email address. If email address is not found, the email address will be added to a blocklist and prevent it from being used in the future.
 * Output-Formats: [*_/_*]
 */
Route::post('/crm/v3/objects/contacts/gdpr-delete', 'GDPRController@postCrmV3ObjectsContactsGdprDelete');
/**
 * post postCrmV3ObjectsContactsSearchDoSearch
 * Summary: 
 * Notes: 
 * Output-Formats: [application/json, *_/_*]
 */
Route::post('/crm/v3/objects/contacts/search', 'SearchController@postCrmV3ObjectsContactsSearchDoSearch');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
