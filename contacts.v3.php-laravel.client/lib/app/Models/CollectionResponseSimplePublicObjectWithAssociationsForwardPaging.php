<?php
/**
 * CollectionResponseSimplePublicObjectWithAssociationsForwardPaging
 */
namespace app\Models;

/**
 * CollectionResponseSimplePublicObjectWithAssociationsForwardPaging
 */
class CollectionResponseSimplePublicObjectWithAssociationsForwardPaging {

    /** @var \app\Models\SimplePublicObjectWithAssociations[] $results */
    private $results;

    /** @var \app\Models\ForwardPaging $paging */
    private $paging;

}
