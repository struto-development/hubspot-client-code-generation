<?php
/**
 * ErrorDetail
 */
namespace app\Models;

/**
 * ErrorDetail
 */
class ErrorDetail {

    /** @var string $message A human readable message describing the error along with remediation steps where appropriate*/
    private $message;

    /** @var string $in The name of the field or parameter in which the error was found.*/
    private $in;

    /** @var string $code The status code associated with the error detail*/
    private $code;

    /** @var string $subCategory A specific category that contains more specific detail about the error*/
    private $subCategory;

    /** @var array<string,string[]> $context Context about the error condition*/
    private $context;

}
