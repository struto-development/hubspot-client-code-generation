<?php
/**
 * StandardError
 */
namespace app\Models;

/**
 * StandardError
 */
class StandardError {

    /** @var string $status */
    private $status;

    /** @var string $id */
    private $id;

    /** @var \app\Models\ErrorCategory $category */
    private $category;

    /** @var object $subCategory */
    private $subCategory;

    /** @var string $message */
    private $message;

    /** @var \app\Models\ErrorDetail[] $errors */
    private $errors;

    /** @var array<string,string[]> $context */
    private $context;

    /** @var array<string,string> $links */
    private $links;

}
