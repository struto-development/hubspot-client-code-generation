<?php
/**
 * CollectionResponseWithTotalSimplePublicObjectForwardPaging
 */
namespace app\Models;

/**
 * CollectionResponseWithTotalSimplePublicObjectForwardPaging
 */
class CollectionResponseWithTotalSimplePublicObjectForwardPaging {

    /** @var int $total */
    private $total;

    /** @var \app\Models\SimplePublicObject[] $results */
    private $results;

    /** @var \app\Models\ForwardPaging $paging */
    private $paging;

}
