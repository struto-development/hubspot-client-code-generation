<?php
/**
 * SimplePublicObject
 */
namespace app\Models;

/**
 * SimplePublicObject
 */
class SimplePublicObject {

    /** @var string $id */
    private $id;

    /** @var array<string,string> $properties */
    private $properties;

    /** @var \DateTime $createdAt */
    private $createdAt;

    /** @var \DateTime $updatedAt */
    private $updatedAt;

    /** @var bool $archived */
    private $archived;

    /** @var \DateTime $archivedAt */
    private $archivedAt;

}
