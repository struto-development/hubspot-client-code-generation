<?php
/**
 * PublicObjectSearchRequest
 */
namespace app\Models;

/**
 * PublicObjectSearchRequest
 */
class PublicObjectSearchRequest {

    /** @var \app\Models\FilterGroup[] $filterGroups */
    private $filterGroups;

    /** @var string[] $sorts */
    private $sorts;

    /** @var string $query */
    private $query;

    /** @var string[] $properties */
    private $properties;

    /** @var int $limit */
    private $limit;

    /** @var int $after */
    private $after;

}
