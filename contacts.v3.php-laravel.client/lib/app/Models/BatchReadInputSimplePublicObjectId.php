<?php
/**
 * BatchReadInputSimplePublicObjectId
 */
namespace app\Models;

/**
 * BatchReadInputSimplePublicObjectId
 */
class BatchReadInputSimplePublicObjectId {

    /** @var string[] $properties */
    private $properties;

    /** @var string $idProperty */
    private $idProperty;

    /** @var \app\Models\SimplePublicObjectId[] $inputs */
    private $inputs;

}
