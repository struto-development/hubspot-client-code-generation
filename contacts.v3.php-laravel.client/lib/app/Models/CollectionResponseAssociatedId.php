<?php
/**
 * CollectionResponseAssociatedId
 */
namespace app\Models;

/**
 * CollectionResponseAssociatedId
 */
class CollectionResponseAssociatedId {

    /** @var \app\Models\AssociatedId[] $results */
    private $results;

    /** @var \app\Models\Paging $paging */
    private $paging;

}
