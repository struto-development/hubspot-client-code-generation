<?php
/**
 * CollectionResponseAssociatedIdForwardPaging
 */
namespace app\Models;

/**
 * CollectionResponseAssociatedIdForwardPaging
 */
class CollectionResponseAssociatedIdForwardPaging {

    /** @var \app\Models\AssociatedId[] $results */
    private $results;

    /** @var \app\Models\ForwardPaging $paging */
    private $paging;

}
