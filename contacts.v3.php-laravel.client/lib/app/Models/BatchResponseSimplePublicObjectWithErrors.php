<?php
/**
 * BatchResponseSimplePublicObjectWithErrors
 */
namespace app\Models;

/**
 * BatchResponseSimplePublicObjectWithErrors
 */
class BatchResponseSimplePublicObjectWithErrors {

    /** @var string $status */
    private $status;

    /** @var \app\Models\SimplePublicObject[] $results */
    private $results;

    /** @var int $numErrors */
    private $numErrors;

    /** @var \app\Models\StandardError[] $errors */
    private $errors;

    /** @var \DateTime $requestedAt */
    private $requestedAt;

    /** @var \DateTime $startedAt */
    private $startedAt;

    /** @var \DateTime $completedAt */
    private $completedAt;

    /** @var array<string,string> $links */
    private $links;

}
