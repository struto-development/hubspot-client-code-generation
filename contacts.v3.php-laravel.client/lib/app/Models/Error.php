<?php
/**
 * Error
 */
namespace app\Models;

/**
 * Error
 */
class Error {

    /** @var string $message A human readable message describing the error along with remediation steps where appropriate*/
    private $message;

    /** @var string $correlationId A unique identifier for the request. Include this value with any error reports or support tickets*/
    private $correlationId;

    /** @var string $category The error category*/
    private $category;

    /** @var string $subCategory A specific category that contains more specific detail about the error*/
    private $subCategory;

    /** @var \app\Models\ErrorDetail[] $errors further information about the error*/
    private $errors;

    /** @var array<string,string[]> $context Context about the error condition*/
    private $context;

    /** @var array<string,string> $links A map of link names to associated URIs containing documentation about the error or recommended remediation steps*/
    private $links;

}
