<?php
/**
 * BatchResponseSimplePublicObject
 */
namespace app\Models;

/**
 * BatchResponseSimplePublicObject
 */
class BatchResponseSimplePublicObject {

    /** @var string $status */
    private $status;

    /** @var \app\Models\SimplePublicObject[] $results */
    private $results;

    /** @var \DateTime $requestedAt */
    private $requestedAt;

    /** @var \DateTime $startedAt */
    private $startedAt;

    /** @var \DateTime $completedAt */
    private $completedAt;

    /** @var array<string,string> $links */
    private $links;

}
