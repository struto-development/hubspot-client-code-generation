# Hubspot API code generation

as per https://community.Hubspot.com/t5/Developer-Announcements/Announcing-a-new-endpoint-that-returns-all-publicly-available/td-p/415566

Hubspot published its API definition in Swagger/OpenAPI format, which allows us
to use automated code generation.

OpenAPI definition file are listed here:

https://api.Hubspot.com/api-catalog-public/v1/apis

Code generation command line utility:

https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.3.0/openapi-generator-cli-5.3.0.jar

Note, Sept 2022 - for more recent versions (current version is 6.1.0), see https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/ page.

## Usage on Windows

java -jar openapi-generator-cli.jar openapi-generator-cli <command> [<args>]


Commands:

    author        Utilities for authoring generators or customizing templates.

    batch         Generate code in batch via external configs.

    config-help   Config help for chosen lang

    generate      Generate code with the specified generator.

    help          Display help information about openapi-generator

    list          Lists the available generators

    meta          MetaGenerator. Generator for creating a new template set and configuration for Codegen.

    validate      Validate specification

    version       Show version information used in tooling

See 'openapi-generator-cli help <command>' for more information on a specific command.

## Client generation

Hubspot API clients can be auto-generated for the following languages:

    - ada
    - android
    - apex
    - bash
    - c
    - clojure
    - cpp-qt-client
    - cpp-restsdk
    - cpp-tiny (beta)
    - cpp-tizen
    - cpp-ue4 (beta)
    - crystal (beta)
    - csharp
    - csharp-netcore
    - dart
    - dart-dio
    - dart-dio-next (experimental)
    - eiffel
    - elixir
    - elm
    - erlang-client
    - erlang-proper
    - go
    - groovy
    - haskell-http-client
    - java
    - java-micronaut-client (beta)
    - javascript
    - javascript-apollo (beta)
    - javascript-closure-angular
    - javascript-flowtyped
    - jaxrs-cxf-client
    - jmeter
    - k6 (beta)
    - kotlin
    - lua (beta)
    - nim (beta)
    - objc
    - ocaml
    - perl
    - php
    - php-dt (beta)
    - powershell (beta)
    - python (experimental)
    - python-legacy
    - r
    - ruby
    - rust
    - scala-akka
    - scala-gatling
    - scala-sttp (beta)
    - scalaz
    - swift5
    - typescript (experimental)
    - typescript-angular
    - typescript-aurelia
    - typescript-axios
    - typescript-fetch
    - typescript-inversify
    - typescript-jquery
    - typescript-nestjs (experimental)
    - typescript-node
    - typescript-redux-query
    - typescript-rxjs

# Server side generation

Generating full Hubspot server code is impossible because of the unknown db layer,
but all API controllers Hubspot uses can be stubbed in a variety of languages.

Utility also has a SCHEMA auto-generator for sql, graphql etc. - however ORM layer
(queries serving various API points) would still be missing.

If it is ever needed to create Hubspot proxy, this would be handy (Hubspot API
calls would be then effected from the auto-generated controllers, but payload
would be served from the generated server app)

This would be ideal approach if Hubspot API needs to be supplemented.

Available SERVER generators:

    - ada-server
    - aspnetcore
    - cpp-pistache-server
    - cpp-qt-qhttpengine-server
    - cpp-restbed-server
    - csharp-nancyfx
    - erlang-server
    - fsharp-functions (beta)
    - fsharp-giraffe-server (beta)
    - go-echo-server (beta)
    - go-gin-server
    - go-server
    - graphql-nodejs-express-server
    - haskell
    - haskell-yesod (beta)
    - java-inflector
    - java-msf4j
    - java-pkmst
    - java-play-framework
    - java-undertow-server
    - java-vertx-web (beta)
    - jaxrs-cxf
    - jaxrs-cxf-cdi
    - jaxrs-cxf-extended
    - jaxrs-jersey
    - jaxrs-resteasy
    - jaxrs-resteasy-eap
    - jaxrs-spec
    - kotlin-server
    - kotlin-spring
    - kotlin-vertx (beta)
    - nodejs-express-server (beta)
    - php-laravel
    - php-lumen
    - php-mezzio-ph
    - php-slim4
    - php-symfony
    - python-aiohttp
    - python-blueplanet
    - python-fastapi (beta)
    - python-flask
    - ruby-on-rails
    - ruby-sinatra
    - rust-server
    - scala-akka-http-server (beta)
    - scala-finch
    - scala-lagom-server
    - scala-play-server
    - scalatra
    - spring

## Other generators

DOCUMENTATION generators:
    - asciidoc
    - cwiki
    - dynamic-html
    - html
    - html2
    - markdown (beta)
    - openapi
    - openapi-yaml
    - plantuml (beta)


SCHEMA generators:
    - avro-schema (beta)
    - graphql-schema
    - ktorm-schema (beta)
    - mysql-schema
    - protobuf-schema (beta)
    - wsdl-schema (beta)


CONFIG generators:
    - apache2


## Hubspot code generation:

### Hubspot contacts

These are command line examples used to generate this repo:

```java -jar openapi-generator-cli.jar generate -i contacts_v3.json -g javascript -o contacts.v3.javascript.client```

```java -jar openapi-generator-cli.jar generate -i contacts_v3.json -g typescript-axios -o contacts.v3.typescript-axios.client```

```java -jar openapi-generator-cli.jar generate -i contacts_v3.json -g php -o contacts.v3.php.client```

```java -jar openapi-generator-cli.jar generate -i contacts_v3.json -g php-laravel -o contacts.v3.php-laravel.client```

## Reading

- [Generate java client from openapi.json](https://javahowtos.com/guides/118-tools/430-easily-generate-java-client-from-swagger-file.html)

- [Postman collection from openapi.json](https://javahowtos.com/guides/118-tools/431-generate-postman-collection-from-swagger-file.html)

- [Acuity Trading](https://api.acuitytrading.com/swagger/index.html)