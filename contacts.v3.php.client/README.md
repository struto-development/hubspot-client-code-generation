# OpenAPIClient-php

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: hapikey
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('hapikey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('hapikey', 'Bearer');

// Configure OAuth2 access token for authorization: oauth2
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2_legacy
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AssociationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$contact_id = 'contact_id_example'; // string
$to_object_type = 'to_object_type_example'; // string
$to_object_id = 'to_object_id_example'; // string
$association_type = 'association_type_example'; // string

try {
    $apiInstance->deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive($contact_id, $to_object_type, $to_object_id, $association_type);
} catch (Exception $e) {
    echo 'Exception when calling AssociationsApi->deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.hubapi.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AssociationsApi* | [**deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive**](docs/Api/AssociationsApi.md#deletecrmv3objectscontactscontactidassociationstoobjecttypetoobjectidassociationtypearchive) | **DELETE** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Remove an association between two contacts
*AssociationsApi* | [**getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll**](docs/Api/AssociationsApi.md#getcrmv3objectscontactscontactidassociationstoobjecttypegetall) | **GET** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType} | List associations of a contact by type
*AssociationsApi* | [**putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate**](docs/Api/AssociationsApi.md#putcrmv3objectscontactscontactidassociationstoobjecttypetoobjectidassociationtypecreate) | **PUT** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Associate a contact with another object
*BasicApi* | [**deleteCrmV3ObjectsContactsContactIdArchive**](docs/Api/BasicApi.md#deletecrmv3objectscontactscontactidarchive) | **DELETE** /crm/v3/objects/contacts/{contactId} | Archive
*BasicApi* | [**getCrmV3ObjectsContactsContactIdGetById**](docs/Api/BasicApi.md#getcrmv3objectscontactscontactidgetbyid) | **GET** /crm/v3/objects/contacts/{contactId} | Read
*BasicApi* | [**getCrmV3ObjectsContactsGetPage**](docs/Api/BasicApi.md#getcrmv3objectscontactsgetpage) | **GET** /crm/v3/objects/contacts | List
*BasicApi* | [**patchCrmV3ObjectsContactsContactIdUpdate**](docs/Api/BasicApi.md#patchcrmv3objectscontactscontactidupdate) | **PATCH** /crm/v3/objects/contacts/{contactId} | Update
*BasicApi* | [**postCrmV3ObjectsContactsCreate**](docs/Api/BasicApi.md#postcrmv3objectscontactscreate) | **POST** /crm/v3/objects/contacts | Create
*BatchApi* | [**postCrmV3ObjectsContactsBatchArchiveArchive**](docs/Api/BatchApi.md#postcrmv3objectscontactsbatcharchivearchive) | **POST** /crm/v3/objects/contacts/batch/archive | Archive a batch of contacts by ID
*BatchApi* | [**postCrmV3ObjectsContactsBatchCreateCreate**](docs/Api/BatchApi.md#postcrmv3objectscontactsbatchcreatecreate) | **POST** /crm/v3/objects/contacts/batch/create | Create a batch of contacts
*BatchApi* | [**postCrmV3ObjectsContactsBatchReadRead**](docs/Api/BatchApi.md#postcrmv3objectscontactsbatchreadread) | **POST** /crm/v3/objects/contacts/batch/read | Read a batch of contacts by internal ID, or unique property values
*BatchApi* | [**postCrmV3ObjectsContactsBatchUpdateUpdate**](docs/Api/BatchApi.md#postcrmv3objectscontactsbatchupdateupdate) | **POST** /crm/v3/objects/contacts/batch/update | Update a batch of contacts
*GDPRApi* | [**postCrmV3ObjectsContactsGdprDelete**](docs/Api/GDPRApi.md#postcrmv3objectscontactsgdprdelete) | **POST** /crm/v3/objects/contacts/gdpr-delete | GDPR DELETE
*SearchApi* | [**postCrmV3ObjectsContactsSearchDoSearch**](docs/Api/SearchApi.md#postcrmv3objectscontactssearchdosearch) | **POST** /crm/v3/objects/contacts/search | 

## Models

- [AssociatedId](docs/Model/AssociatedId.md)
- [BatchInputSimplePublicObjectBatchInput](docs/Model/BatchInputSimplePublicObjectBatchInput.md)
- [BatchInputSimplePublicObjectId](docs/Model/BatchInputSimplePublicObjectId.md)
- [BatchInputSimplePublicObjectInput](docs/Model/BatchInputSimplePublicObjectInput.md)
- [BatchReadInputSimplePublicObjectId](docs/Model/BatchReadInputSimplePublicObjectId.md)
- [BatchResponseSimplePublicObject](docs/Model/BatchResponseSimplePublicObject.md)
- [BatchResponseSimplePublicObjectWithErrors](docs/Model/BatchResponseSimplePublicObjectWithErrors.md)
- [CollectionResponseAssociatedId](docs/Model/CollectionResponseAssociatedId.md)
- [CollectionResponseAssociatedIdForwardPaging](docs/Model/CollectionResponseAssociatedIdForwardPaging.md)
- [CollectionResponseSimplePublicObjectWithAssociationsForwardPaging](docs/Model/CollectionResponseSimplePublicObjectWithAssociationsForwardPaging.md)
- [CollectionResponseWithTotalSimplePublicObjectForwardPaging](docs/Model/CollectionResponseWithTotalSimplePublicObjectForwardPaging.md)
- [Error](docs/Model/Error.md)
- [ErrorCategory](docs/Model/ErrorCategory.md)
- [ErrorDetail](docs/Model/ErrorDetail.md)
- [Filter](docs/Model/Filter.md)
- [FilterGroup](docs/Model/FilterGroup.md)
- [ForwardPaging](docs/Model/ForwardPaging.md)
- [NextPage](docs/Model/NextPage.md)
- [Paging](docs/Model/Paging.md)
- [PreviousPage](docs/Model/PreviousPage.md)
- [PublicGdprDeleteInput](docs/Model/PublicGdprDeleteInput.md)
- [PublicObjectSearchRequest](docs/Model/PublicObjectSearchRequest.md)
- [SimplePublicObject](docs/Model/SimplePublicObject.md)
- [SimplePublicObjectBatchInput](docs/Model/SimplePublicObjectBatchInput.md)
- [SimplePublicObjectId](docs/Model/SimplePublicObjectId.md)
- [SimplePublicObjectInput](docs/Model/SimplePublicObjectInput.md)
- [SimplePublicObjectWithAssociations](docs/Model/SimplePublicObjectWithAssociations.md)
- [StandardError](docs/Model/StandardError.md)

## Authorization

### hapikey

- **Type**: API key
- **API key parameter name**: hapikey
- **Location**: URL query string



### oauth2

- **Type**: `OAuth`
- **Flow**: `accessCode`
- **Authorization URL**: `https://app.hubspot.com/oauth/authorize`
- **Scopes**: 
    - **crm.objects.contacts.read**: 
    - **crm.objects.contacts.write**: 


### oauth2_legacy

- **Type**: `OAuth`
- **Flow**: `accessCode`
- **Authorization URL**: `https://app.hubspot.com/oauth/authorize`
- **Scopes**: 
    - **contacts**: Read from and write to my Contacts

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `v3`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
