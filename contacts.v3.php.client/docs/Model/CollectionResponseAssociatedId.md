# # CollectionResponseAssociatedId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**\OpenAPI\Client\Model\AssociatedId[]**](AssociatedId.md) |  |
**paging** | [**\OpenAPI\Client\Model\Paging**](Paging.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
