# # ErrorDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** | A human readable message describing the error along with remediation steps where appropriate |
**in** | **string** | The name of the field or parameter in which the error was found. | [optional]
**code** | **string** | The status code associated with the error detail | [optional]
**sub_category** | **string** | A specific category that contains more specific detail about the error | [optional]
**context** | **array<string,string[]>** | Context about the error condition | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
