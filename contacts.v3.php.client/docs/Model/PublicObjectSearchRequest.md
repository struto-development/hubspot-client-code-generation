# # PublicObjectSearchRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter_groups** | [**\OpenAPI\Client\Model\FilterGroup[]**](FilterGroup.md) |  |
**sorts** | **string[]** |  |
**query** | **string** |  | [optional]
**properties** | **string[]** |  |
**limit** | **int** |  |
**after** | **int** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
