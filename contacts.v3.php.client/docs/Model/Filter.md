# # Filter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** |  | [optional]
**property_name** | **string** |  |
**operator** | **string** | null |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
