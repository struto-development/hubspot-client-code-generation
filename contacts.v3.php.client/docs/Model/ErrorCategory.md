# # ErrorCategory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**http_status** | **string** |  |
**name** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
