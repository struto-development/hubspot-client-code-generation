# # CollectionResponseSimplePublicObjectWithAssociationsForwardPaging

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**\OpenAPI\Client\Model\SimplePublicObjectWithAssociations[]**](SimplePublicObjectWithAssociations.md) |  |
**paging** | [**\OpenAPI\Client\Model\ForwardPaging**](ForwardPaging.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
