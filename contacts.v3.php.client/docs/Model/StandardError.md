# # StandardError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  |
**id** | **string** |  | [optional]
**category** | [**\OpenAPI\Client\Model\ErrorCategory**](ErrorCategory.md) |  |
**sub_category** | **object** |  | [optional]
**message** | **string** |  |
**errors** | [**\OpenAPI\Client\Model\ErrorDetail[]**](ErrorDetail.md) |  |
**context** | **array<string,string[]>** |  |
**links** | **array<string,string>** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
