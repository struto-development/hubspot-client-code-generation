# # PublicGdprDeleteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object_id** | **string** |  |
**id_property** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
