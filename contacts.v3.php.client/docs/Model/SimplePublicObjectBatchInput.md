# # SimplePublicObjectBatchInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | **array<string,string>** |  |
**id** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
