# # BatchInputSimplePublicObjectInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**\OpenAPI\Client\Model\SimplePublicObjectInput[]**](SimplePublicObjectInput.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
