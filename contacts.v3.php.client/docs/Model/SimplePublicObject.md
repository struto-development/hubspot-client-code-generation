# # SimplePublicObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  |
**properties** | **array<string,string>** |  |
**created_at** | **\DateTime** |  |
**updated_at** | **\DateTime** |  |
**archived** | **bool** |  | [optional]
**archived_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
