# # BatchResponseSimplePublicObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  |
**results** | [**\OpenAPI\Client\Model\SimplePublicObject[]**](SimplePublicObject.md) |  |
**requested_at** | **\DateTime** |  | [optional]
**started_at** | **\DateTime** |  |
**completed_at** | **\DateTime** |  |
**links** | **array<string,string>** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
