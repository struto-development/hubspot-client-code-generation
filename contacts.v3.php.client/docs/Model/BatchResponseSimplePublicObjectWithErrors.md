# # BatchResponseSimplePublicObjectWithErrors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  |
**results** | [**\OpenAPI\Client\Model\SimplePublicObject[]**](SimplePublicObject.md) |  |
**num_errors** | **int** |  | [optional]
**errors** | [**\OpenAPI\Client\Model\StandardError[]**](StandardError.md) |  | [optional]
**requested_at** | **\DateTime** |  | [optional]
**started_at** | **\DateTime** |  |
**completed_at** | **\DateTime** |  |
**links** | **array<string,string>** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
