# OpenAPI\Client\AssociationsApi

All URIs are relative to https://api.hubapi.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive()**](AssociationsApi.md#deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive) | **DELETE** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Remove an association between two contacts
[**getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll()**](AssociationsApi.md#getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll) | **GET** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType} | List associations of a contact by type
[**putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate()**](AssociationsApi.md#putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate) | **PUT** /crm/v3/objects/contacts/{contactId}/associations/{toObjectType}/{toObjectId}/{associationType} | Associate a contact with another object


## `deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive()`

```php
deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive($contact_id, $to_object_type, $to_object_id, $association_type)
```

Remove an association between two contacts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: hapikey
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('hapikey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('hapikey', 'Bearer');

// Configure OAuth2 access token for authorization: oauth2
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2_legacy
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AssociationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$contact_id = 'contact_id_example'; // string
$to_object_type = 'to_object_type_example'; // string
$to_object_id = 'to_object_id_example'; // string
$association_type = 'association_type_example'; // string

try {
    $apiInstance->deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive($contact_id, $to_object_type, $to_object_id, $association_type);
} catch (Exception $e) {
    echo 'Exception when calling AssociationsApi->deleteCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeArchive: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**|  |
 **to_object_type** | **string**|  |
 **to_object_id** | **string**|  |
 **association_type** | **string**|  |

### Return type

void (empty response body)

### Authorization

[hapikey](../../README.md#hapikey), [oauth2](../../README.md#oauth2), [oauth2_legacy](../../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `*/*`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll()`

```php
getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll($contact_id, $to_object_type, $after, $limit): \OpenAPI\Client\Model\CollectionResponseAssociatedIdForwardPaging
```

List associations of a contact by type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: hapikey
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('hapikey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('hapikey', 'Bearer');

// Configure OAuth2 access token for authorization: oauth2
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2_legacy
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AssociationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$contact_id = 'contact_id_example'; // string
$to_object_type = 'to_object_type_example'; // string
$after = 'after_example'; // string | The paging cursor token of the last successfully read resource will be returned as the `paging.next.after` JSON property of a paged response containing more results.
$limit = 500; // int | The maximum number of results to display per page.

try {
    $result = $apiInstance->getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll($contact_id, $to_object_type, $after, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AssociationsApi->getCrmV3ObjectsContactsContactIdAssociationsToObjectTypeGetAll: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**|  |
 **to_object_type** | **string**|  |
 **after** | **string**| The paging cursor token of the last successfully read resource will be returned as the &#x60;paging.next.after&#x60; JSON property of a paged response containing more results. | [optional]
 **limit** | **int**| The maximum number of results to display per page. | [optional] [default to 500]

### Return type

[**\OpenAPI\Client\Model\CollectionResponseAssociatedIdForwardPaging**](../Model/CollectionResponseAssociatedIdForwardPaging.md)

### Authorization

[hapikey](../../README.md#hapikey), [oauth2](../../README.md#oauth2), [oauth2_legacy](../../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`, `*/*`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate()`

```php
putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate($contact_id, $to_object_type, $to_object_id, $association_type): \OpenAPI\Client\Model\SimplePublicObjectWithAssociations
```

Associate a contact with another object

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: hapikey
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('hapikey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('hapikey', 'Bearer');

// Configure OAuth2 access token for authorization: oauth2
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2_legacy
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AssociationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$contact_id = 'contact_id_example'; // string
$to_object_type = 'to_object_type_example'; // string
$to_object_id = 'to_object_id_example'; // string
$association_type = 'association_type_example'; // string

try {
    $result = $apiInstance->putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate($contact_id, $to_object_type, $to_object_id, $association_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AssociationsApi->putCrmV3ObjectsContactsContactIdAssociationsToObjectTypeToObjectIdAssociationTypeCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**|  |
 **to_object_type** | **string**|  |
 **to_object_id** | **string**|  |
 **association_type** | **string**|  |

### Return type

[**\OpenAPI\Client\Model\SimplePublicObjectWithAssociations**](../Model/SimplePublicObjectWithAssociations.md)

### Authorization

[hapikey](../../README.md#hapikey), [oauth2](../../README.md#oauth2), [oauth2_legacy](../../README.md#oauth2_legacy)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`, `*/*`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
